import nzae
import math
import numpy
from PatternMatching import PatternMatching

    
class PM_GetCostAccuracy(nzae.Ae):

    SCWindow = 6
    Equity = [ 0.0 ]
    MinutesInADay = 24 * 60 * 60

    def _runUdtf(self):

        for row in self:
            
            processdate, startdate, pnldate, dailypnl, rown = row

            if rown == 1:
                self.Equity = [ 0.0 ]


            if pnldate >= startdate and pnldate < processdate:
                testdate = pnldate
                testPL = float(dailypnl)
                
                sequence = self.Equity[1:] #EXCLUDE STARTING 0.0
                last60days = sequence[-60:]
                pm = PatternMatching(last60days, self.SCWindow, float('inf'))
                Matches = pm.getMatchedPatterns(sequence)

                Current_CostPL = []
                for match in Matches:
                    """ GET NEXT DAY PL """
                    indexAfterPattern = match['index'][-1] + 1
                    if indexAfterPattern < len(sequence):
                        PL = sequence[indexAfterPattern] - sequence[indexAfterPattern-1]
                        """ GET COST """     
                        cost = float( math.ceil(match['cost']*10000)/10000 )
                        """ APPEND TO LIST """
                        Current_CostPL.append( [cost, PL, PL] )

                """ SORT ORGANIZED DATA BY COST """
                Current_CostPL.sort(key=lambda tup: tup[0])

                """ FILL IN ORGANIZED DATA """
                """ FIRST RECORD """
                if (testPL > 0 and Current_CostPL[0][2] > 0) or (testPL <= 0 and Current_CostPL[0][2] <= 0):
                    self.output( Current_CostPL[0][0], testdate, 1 )
                else:
                    self.output( Current_CostPL[0][0], testdate, 0 )
                """ OTHERS """
                for i in range(1, len(Current_CostPL)):
                    Current_CostPL[i][2] += Current_CostPL[i-1][2]
                    if (testPL > 0 and Current_CostPL[i][2] > 0) or (testPL <= 0 and Current_CostPL[i][2] <= 0):
                        self.output( Current_CostPL[i][0], testdate, 1 )
                    else:
                        self.output( Current_CostPL[i][0], testdate, 0 )
                

            #EXCLUDE THE CURRENT DAY
            self.Equity.append( self.Equity[-1] + float(dailypnl) )
                    
PM_GetCostAccuracy.run()
