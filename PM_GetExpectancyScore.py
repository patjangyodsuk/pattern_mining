import nzae
from PatternMatching import PatternMatching


class PM_GetExpectancyScore(nzae.Ae):

    SCWindow = 6
    DateList = [ 0 ]
    Equity = [ 0.0 ]

    def _runUdtf(self):

        for row in self:
            
            startdate, pnldate, dailypnl, threshold, rown, totalrow = row

            if rown == 1:
                self.SCWindow = 6
                self.DateList = [ 0 ]
                self.Equity = [ 0.0 ]


            self.DateList.append( pnldate )
            self.Equity.append( self.Equity[-1] + float(dailypnl) )

            
            if rown == totalrow:

                """ FIND START PROCESSING DATE """
                startindex = 1
                while startindex < len(self.DateList) and self.DateList[startindex] < startdate:
                    startindex += 1
                    

                d = startindex
                while d < len(self.Equity):
                    sequence = self.Equity[1:d]
                    last60days = sequence[-60:]
                    pm = PatternMatching(last60days, self.SCWindow, float(threshold))
                    Matches = pm.getMatchedPatterns(sequence)

                    """ CALCULATE PROBABILITY OF PROFIT """
                    ProbProfit = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                    Nmatches = 0
                    for match in Matches:
                        lastIndex = match['index'][-1]
                        if (lastIndex + 10) < len(sequence):
                            for j in range(0,10):
                                if (sequence[lastIndex+j+1] - sequence[lastIndex]) > 0.0:
                                    ProbProfit[j] += 1
                            Nmatches += 1
                    
                    if Nmatches > 0:

                        """ AVERAGE PROBABILITY OF PROFIT """
                        for i in range(0,10):
                            ProbProfit[i] /= Nmatches

                        """ FIND THE BEST WAIT TIME (PROFIT) = MAX PROBABILITY OF PROFIT (PREFER THE LONGEST) """
                        maxProbProfit = 0.0
                        for i in range(9,-1,-1):
                            if ProbProfit[i] > maxProbProfit: # and ProbProfit[i] > 0:
                                maxProbProfit = ProbProfit[i]
                                bestWaitBullTime = i+1

                        """ FIND THE BEST WAIT TIME (LOSS) = MIN PROBABILITY OF PROFIT (PREFER THE LONGEST) """
                        minProProfit = float('inf')
                        for i in range(9,-1,-1):
                            if ProbProfit[i] < minProProfit: # and ProbProfit[i] > 0:
                                minProProfit = ProbProfit[i]
                                bestWaitBearTime = i+1
                            
                        if maxProbProfit > 0.55:
                            #self.output( self.DateList[d], ProbProfit[0], ProbProfit[1], ProbProfit[2], ProbProfit[3], ProbProfit[4], ProbProfit[5], ProbProfit[6], ProbProfit[7], ProbProfit[8], ProbProfit[9], 1)
                            """ GET EQUITY CURVE """
                            for i in range(0, min(bestWaitBullTime, len(self.DateList)-d)):
                                self.output( self.DateList[d+i], 1, threshold )
                            """ WAIT FOR BULL PERIOD: bestWaitBullTime DAYS """
                            d += bestWaitBullTime
                        elif minProProfit < 0.45:
                            #self.output( self.DateList[d], ProbProfit[0], ProbProfit[1], ProbProfit[2], ProbProfit[3], ProbProfit[4], ProbProfit[5], ProbProfit[6], ProbProfit[7], ProbProfit[8], ProbProfit[9], -1)
                            """ GET EQUITY CURVE """
                            for i in range(0, min(bestWaitBearTime, len(self.DateList)-d)):
                                self.output( self.DateList[d+i], -1, threshold )
                            """ WAIT FOR BEAR PERIOD: bestWaitBearTime DAYS """
                            d += bestWaitBearTime
                        else:
                            #self.output( self.DateList[d], ProbProfit[0], ProbProfit[1], ProbProfit[2], ProbProfit[3], ProbProfit[4], ProbProfit[5], ProbProfit[6], ProbProfit[7], ProbProfit[8], ProbProfit[9], 0)
                            """ GET EQUITY CURVE """
                            for i in range(0, min(10, len(self.DateList)-d)):
                                self.output( self.DateList[d+i], 0, threshold )
                            """ INDECISIVE: TRY AGAIN THE NEXT 10 DAY """
                            d += 10
                    else:
                        #self.output( self.DateList[d], ProbProfit[0], ProbProfit[1], ProbProfit[2], ProbProfit[3], ProbProfit[4], ProbProfit[5], ProbProfit[6], ProbProfit[7], ProbProfit[8], ProbProfit[9], 0)
                        """ GET EQUITY CURVE """
                        self.output( self.DateList[d], 0, threshold )
                        """ CANNOT FIND MATCHED PATTERNS: TRY AGAIN THE NEXT DAY """
                        d += 1



PM_GetExpectancyScore.run()

