import os
import csv
from datetime import datetime


result_folder = "./Results_QA/CSV"
files = os.listdir(result_folder)
combined_results = {}
Markets = []

for f in files:
    Market = f[:f.index('_')]
    Markets.append( Market )
    data = []
    with open("%s/%s" % (result_folder, f), "rb") as rfile:
        reader = csv.reader(rfile, delimiter=',')
        next(reader, None)
        data = list(reader)

    for record in data:
        dt = datetime.strptime(record[0], "%Y-%m-%d")
        if not dt in combined_results:
            combined_results[ dt ] = {}
        if record[1] != "" and record[1] != " ":
            combined_results[ dt ][ Market ] = int(record[1])
        else:
            combined_results[ dt ][ Market ] = 0


alldate = combined_results.keys()
alldate.sort()
Markets.sort()
with open("combined_QA.csv", "wb") as ofile:
    """ PRINT HEADER """
    for Market in Markets:
        ofile.write(",%s" % (Market))
    """ PRINT RESULT """
    for i in range(0, len(alldate)):
        currrent = alldate[i]
        ofile.write("\n%s" % (currrent.strftime("%Y-%m-%d")))
        for Market in Markets:
            if Market in combined_results[ currrent ]:
                ofile.write(",%d" % (combined_results[ currrent ][ Market ]))
            else:
                p = i - 1
                while not Market in combined_results[ alldate[p] ]:
                    p -= 1
                ofile.write(",%d" % (combined_results[ alldate[p] ][ Market ]))




        
