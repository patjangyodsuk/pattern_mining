INSERT INTO DEV_CAPITALOGIX.ADMIN.PM_CALCULATION_ALLLOOKBACK_PREDICTIONS
SELECT input.SECURITYID, input.PROCESSDAY, input.LOOKBACK, input.BESTTRHESHOLD, '1970-01-01'::date + output.TestDate * interval '1 second' As TestDate, output.OnOff
FROM (
SELECT p.SECURITYID, 
		d.STARTDAY, 
		extract(epoch from d.STARTDAY) as LONGSTARTDAY, 
		p.RPTDAY, 
		extract(epoch from p.RPTDAY) as LONGRPTDAY, 
		p.PNL, 
		t.BESTTRHESHOLD,
		d.PROCESSDAY, 
		d.PROCESSID, 
		t.LOOKBACK, 
		row_number() over (partition by d.PROCESSID order by LONGRPTDAY) as rown, 
		count(*) over (partition by d.PROCESSID) as totalrow
FROM DEV_CAPITALOGIX.ADMIN.MARKETPNL AS p
JOIN DEV_CAPITALOGIX.ADMIN.PM_PROCESSDAY AS d
  ON p.RptDay <= d.PROCESSDAY
JOIN DEV_CAPITALOGIX.ADMIN.PM_PREVIOUS_MONTH AS prev
  ON d.PROCESSDAY = prev.PROCESSDAY
JOIN DEV_CAPITALOGIX.ADMIN.PM_CALCULATION_THRESHOLD AS t
  ON prev.PREVIOUSMONTH = t.FORMOyNTH AND t.LOOKBACK = d.PROCESSTYPE-3 AND p.SECURITYID = t.SECURITYID
WHERE p.SECURITYID = 12 AND d.PROCESSID = 922
ORDER BY t.LOOKBACK, p.RPTDAY --d.PROCESSTYPE >= 4 AND d.PROCESSTYPE <= 6 AND d.PROCESSDAY = '2016-04-30'
) AS input,
	TABLE WITH FINAL
		( PM_GetExpectancyScore(input.LONGSTARTDAY, input.LONGRPTDAY, input.PNL, input.BESTTRHESHOLD, input.rown, input.totalrow) ) AS output;
