import nzae
import math


class Get_Threshold(nzae.Ae):

    

    def _runUdtf(self):

        DateWAccuracy = {}
        DateWPatterns = {}
        Cost2Date2Accuracy = {}

        for row in self:

            testdate, cost, accuracy, rown, totalrown = row

            """ START NEW SET """
            if rown == 1:
                self.DateWAccuracy = {}
                self.DateWPatterns = {}
                self.Cost2Date2Accuracy = {}

            """ GET ORGANIZED DATA """
            if not cost in self.Cost2Date2Accuracy:
                self.Cost2Date2Accuracy.update( {cost: {testdate: accuracy}} )
            else:
                self.Cost2Date2Accuracy[ cost ].update( {testdate: accuracy} )

            """ GET CALCULATION VARIABLE """
            if not testdate in self.DateWAccuracy:
                self.DateWAccuracy.update( {testdate: 0} )
                self.DateWPatterns.update( {testdate: 0} )


            """ GET ALL NEEDED DATA, START CALCULATING THRESHOLD """
            if rown == totalrown:
                
                ThresResults = []
                sortedAllCost = self.Cost2Date2Accuracy.keys()
                sortedAllCost.sort()
                for threshold in sortedAllCost:
                    """ MARK THAT DATE AS PATTERN_FOUND """
                    for date in self.Cost2Date2Accuracy[threshold]:
                        self.DateWAccuracy[date] = self.Cost2Date2Accuracy[threshold][date]
                        self.DateWPatterns[date] = 1
                    """ KEEP RECORDS """
                    ThresResults.append( [threshold, sum(self.DateWAccuracy.values()), sum(self.DateWPatterns.values())] )


                """ GET "BEST" THRESHOLD """
                TestDates = self.DateWAccuracy.keys()
                MinFoundDate = math.ceil(len(TestDates) * 0.8)
                MaxAcceptableCost = int(math.ceil(len(ThresResults) * 0.05))
                BestThreshold = 0
                MaxAccuracy = 0
                for i in range(0, MaxAcceptableCost):
                    result = ThresResults[i]
                    if ThresResults[i][2] >= MinFoundDate:
                        accuracy = float(ThresResults[i][1])/ThresResults[i][2]
                        if accuracy > MaxAccuracy:
                            BestThreshold = ThresResults[i][0]
                            MaxAccuracy = accuracy

                self.output( BestThreshold, MaxAccuracy )

Get_Threshold.run()

