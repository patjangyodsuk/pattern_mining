import math
import pyodbc


connection = pyodbc.connect('DRIVER={NetezzaSQL};SERVER=98.6.73.34;DATABASE=DEV_CAPITALOGIX;UID=pjangyodsuk;PWD=Iolap3goPJ')
cs = connection.cursor()

Market = 'AD'
Years = range(2016, 2017)
Months = range(6, 13)
SCWindow = 6
EndDate = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
MonthName = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']


for year in Years:
    for month in Months:

        TestDates = []
        DateWAccuracy = {}
        DateWPatterns = {}
        Cost2Date2Accuracy = {}
        
        ForMonth = "%d-%02d-01" % (year, month)
        print ("For %s:" % (ForMonth))

        """ RETRIEVE TEST DATA """
        query = """ SELECT Cost, TestDate, Accuracy
                    FROM DEV_CAPITALOGIX.ADMIN.PM_CALCULATION_DAILYACCURACY
                    WHERE SecurityID = 12 AND TestDate LIKE '%d-%02d-%%'
                    ORDER BY TestDate, Cost """ % (year, month)
        cs.execute(query)
        data = cs.fetchall()

        for record in data:
            cost = float( math.ceil(float(record[0])*10000)/10000 )
            testdate = record[1].strftime("%Y-%m-%d")
            accuracy = int(record[2])
            
            if not cost in Cost2Date2Accuracy:
                Cost2Date2Accuracy.update( {cost: {testdate: accuracy}} )
            else:
                Cost2Date2Accuracy[ cost ].update( {testdate: accuracy} )

            if not testdate in TestDates:
                TestDates.append(testdate)
                DateWAccuracy.update( {testdate: 0} )
                DateWPatterns.update( {testdate: 0} )
                

        ThresResults = []
        sortedAllCost = Cost2Date2Accuracy.keys()
        sortedAllCost.sort()
        for threshold in sortedAllCost:
            """ MARK THAT DATE AS PATTERN_FOUND """
            for date in Cost2Date2Accuracy[threshold]:
                DateWAccuracy[date] = Cost2Date2Accuracy[threshold][date]
                DateWPatterns[date] = 1
            """ KEEP RECORDS """
            ThresResults.append( [threshold, sum(DateWAccuracy.values()), sum(DateWPatterns.values())] )



        """ GET "BEST" THRESHOLD """
        MinFoundDate = math.ceil(len(TestDates) * 0.8)
        MaxAcceptableCost = int(math.ceil(len(ThresResults) * 0.05))
        BestThreshold = 0
        MaxAccuracy = 0
        for i in range(0, MaxAcceptableCost):
            result = ThresResults[i]
            if ThresResults[i][2] >= MinFoundDate:
                accuracy = float(ThresResults[i][1])/ThresResults[i][2]
                if accuracy > MaxAccuracy:
                    BestThreshold = ThresResults[i][0]
                    MaxAccuracy = accuracy

        print "%d-%02d: %f" % (year, month, BestThreshold)

cs.close()
del cs
connection.close()
