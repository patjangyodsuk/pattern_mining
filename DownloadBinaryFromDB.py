import pyodbc


MarketIDs = []
ModelID = 2018
Bot_VersionID = 1
LiveOrBackTesting = 'Live'


connection = pyodbc.connect('DRIVER={NetezzaSQL};SERVER=192.168.1.153;DATABASE=RESEARCH;UID=pjangyodsuk;PWD=Iolap3goPJ')
cs = connection.cursor()

query = """ SELECT MARKETID, NAME
            FROM ADMIN.MARKETS
            WHERE MARKETID <= 14 AND MARKETID <> 11
            ORDER BY NAME; """
cs.execute( query )
results = cs.fetchall()
Header = ""
for record in results:
    MarketIDs.append( record[0] )
    Header += ",%s" % (record[1][1:record[1].index('.')])
Header += "\n"


OnOff = {}
for MarketID in MarketIDs:
    query = """ SELECT CAST(TRANSACTIONDATE AS DATE), EXPOSURE
                FROM ADMIN.BOTSTRANSACTIONS AS t
                JOIN ADMIN.BOTS AS b
                  ON t.BOTID = b.BOTID AND t.MARKETID = b.MARKETID
                WHERE t.MARKETID = %d AND BOTNAME LIKE 'PM_%%_%s' AND MODELID = %d AND VERSIONID = %d
                ORDER BY TRANSACTIONDATE; """ % (MarketID, LiveOrBackTesting, ModelID, Bot_VersionID)
    cs.execute( query )
    Transactions = cs.fetchall()

    query = """ SELECT RPTDAY
                FROM ADMIN.MARKETDAILYPNL
                WHERE MARKETID = %d AND RPTDAY >= '2015-01-01'
                ORDER BY RPTDAY; """ % (MarketID)
    cs.execute( query )
    TradingDays = cs.fetchall()

    t = 0
    current_position = 0
    for tradingday in TradingDays:
        """ MOVE POINTER TO TRANACTION """
        if t < len(Transactions) and tradingday[0] >= Transactions[t][0]:
            current_position = Transactions[t][1]
            t += 1

        """ UPDATE ONOFF LIST """
        if not tradingday[0] in OnOff:
            OnOff.update( {tradingday[0]: {MarketID: current_position}} )
        else:
            OnOff[tradingday[0]].update( {MarketID: current_position} )


AllTradingDays = OnOff.keys()
AllTradingDays.sort()

PreviousPosition = {}
for MarketID in MarketIDs:
    PreviousPosition.update( {MarketID: 0} )

with open("Binary_from_DB_Live.csv", "wb") as ofile:
    ofile.write(Header)
    for tradingday in AllTradingDays:
        Positions = []
        for MarketID in MarketIDs:
            if not MarketID in OnOff[ tradingday ]:
                Positions.append( PreviousPosition[MarketID] )
            else:
                Positions.append( OnOff[ tradingday ][ MarketID ] )
                PreviousPosition[MarketID] = OnOff[ tradingday ][ MarketID ]
        ofile.write("%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n" % (tradingday.strftime("%Y-%m-%d"), Positions[0], Positions[1] \
                                                                  , Positions[2], Positions[3], Positions[4], Positions[5] \
                                                                  , Positions[6], Positions[7], Positions[8], Positions[9] \
                                                                  , Positions[10], Positions[11], Positions[12]))


cs.close()
del cs
connection.close()
