import numpy


class ZNorm:

    def __init__(self, data):
        self.data = data
        self.normed_data = []

    def compute(self):
        mu = numpy.mean(self.data)
        sigma = numpy.std(self.data)
        for i in range(0, len(self.data)):
            self.normed_data.append( (self.data[i] - mu) / sigma )
        return self.normed_data
