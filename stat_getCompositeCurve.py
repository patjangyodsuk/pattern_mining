import csv
from datetime import datetime


def calculateCAGR(equity, start, end):
    startdate = datetime.strptime(start, dateformat)
    enddate = datetime.strptime(end, dateformat)
    N = enddate - startdate
    print N
    return ((float(equity[-1]) / initialCapital)**(365.0/N.days)) - 1.0


def calculateRateOfReturn(equity):
    return float(equity[-1] - initialCapital)/initialCapital


def calculateMaxDrawDown(equity):
    peak = equity[0]
    maxDD = -float('inf')
    for i in range(1, len(equity)):
        DD = float(peak - equity[i])/peak
        if DD > maxDD:
            maxDD = DD
        if equity[i] > peak:
            peak = equity[i]
    return maxDD


def calculateAvgDrawDown(equity):
    peak = equity[0]
    trough = float('inf')
    sumDD = 0.0
    countDD = 0            
    for i in range(1, len(equity)):

        if equity[i] > peak:
            if trough < float('inf'):
                sumDD += float(peak - trough)/peak
                countDD += 1
                trough = float('inf')
            peak = equity[i]

        if equity[i] < trough:
            trough = equity[i]

    if trough < float('inf'):
        sumDD += float(peak - trough)/peak
        countDD += 1
            
    return sumDD / countDD


def calculateMARp(equity):
    return calculateRateOfReturn(equity) / calculateMaxDrawDown(equity)


def calculateMAR(equity, start, end):
    return calculateCAGR(equity, start, end) / calculateMaxDrawDown(equity)


def calculateUVI(equity):
    peak = equity[0]
    maxUVI = 0.0
    currentUVI = 0.0
    for i in range(1, len(equity)):
        
        if equity[i] > peak:
            if currentUVI > maxUVI:
                maxUVI = currentUVI
            currentUVI = 0.0
            peak = equity[i]
        else:
            currentUVI += (peak - equity[i])

    if currentUVI > maxUVI:
        maxUVI = currentUVI
            
    return maxUVI
    

initialCapital = 200000.0
Markets = ['BP', 'CD', 'CT', 'HG', 'HO', 'LH', 'RB', 'S', 'SI', 'W']
#Markets = ['AD', 'CL', 'CORN', 'DX', 'EC', 'ES', 'GC', 'JY', 'KC', 'NG', 'SB', 'TY'] #, 'TF'
resultDir = 'Results_QA_10markets'
composite_filename = "composite_10markets_QA.csv"
metricfile = "metric_10markets_QA.csv"
dateformat = "%Y-%m-%d"


print resultDir


CompositeMarketPL = {}
CompositeModelPL = {}

for market in Markets:

    data = []
    with open("./%s/%s_Netezza.csv" % (resultDir, market), "rb") as efile:
        reader = csv.reader(efile, delimiter=',')
        next(reader, None)
        data = list(reader)

    date = datetime.strptime(data[0][0], dateformat)
    if not date in CompositeModelPL:
        CompositeMarketPL.update( {date: float(data[0][3])} )
        CompositeModelPL.update( {date: float(data[0][4])} )
    else:
        CompositeMarketPL[ date ] += float(data[0][3])
        CompositeModelPL[ date ] += float(data[0][4])
        
    for i in range(1, len(data)):
        date = datetime.strptime(data[i][0], dateformat)
        if date == datetime(2016, 2, 29):
            continue
        marketpl = float(data[i][3]) - float(data[i-1][3])
        modelpl = float(data[i][4]) - float(data[i-1][4])
        if not date in CompositeModelPL:
            CompositeMarketPL.update( {date: marketpl} )
            CompositeModelPL.update( {date: modelpl} )
        else:
            CompositeMarketPL[ date ] += marketpl
            CompositeModelPL[ date ] += modelpl

sortedDate = CompositeModelPL.keys()
sortedDate.sort()

startdate = sortedDate[0].strftime(dateformat)
enddate = sortedDate[-1].strftime(dateformat)

accumMarket = initialCapital
accumModel = initialCapital
MarketEquity = [ accumMarket ]
ModelEquity = [ accumModel ]
with open(composite_filename, "wb") as cfile:
    cfile.write("Date,Market,Model\n")
    for date in sortedDate:
        accumMarket += CompositeMarketPL[date]
        MarketEquity.append( accumMarket )
        accumModel += CompositeModelPL[date]
        ModelEquity.append( accumModel )
        cfile.write("%s,%f,%f\n" % (date, accumMarket, accumModel))

print len(MarketEquity)

marketCAGR = calculateCAGR(MarketEquity, startdate, enddate)
marketMAR = calculateMAR(MarketEquity, startdate, enddate)
marketRR = calculateRateOfReturn(MarketEquity)
marketMaxDD = calculateMaxDrawDown(MarketEquity)
marketAvgDD = calculateAvgDrawDown(MarketEquity)
marketUVI = calculateUVI(MarketEquity)

modelCAGR = calculateCAGR(ModelEquity, startdate, enddate)
modelMAR = calculateMAR(ModelEquity, startdate, enddate)
modelRR = calculateRateOfReturn(ModelEquity)
modelMaxDD = calculateMaxDrawDown(ModelEquity)
modelAvgDD = calculateAvgDrawDown(ModelEquity)
modelUVI = calculateUVI(ModelEquity)

with open(metricfile, "wb") as ofile:
    ofile.write(",CAGR,MAR,RR,MaxDD,AvgDD,UVI\n")
    ofile.write("Market,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f\n" % (marketCAGR, marketMAR, marketRR, marketMaxDD, marketAvgDD, marketUVI))
    ofile.write("Model,%.5f,%.5f,%.5f,%.5f,%.5f,%.5f\n" % (modelCAGR, modelMAR, modelRR, modelMaxDD, modelAvgDD, modelUVI))

