import time
import pyodbc
from datetime import datetime
import matplotlib.pyplot as plt
plt.switch_backend('agg')
from matplotlib.backends.backend_pdf import PdfPages


resultDir = "./Results_QA_10markets"
SecurityIDs = range(15, 25)
Markets = ['', 'ES', 'EC', 'CL', 'GC', 'TF', 'TY', 'DX', 'NG', 'JY', 'Corn', 'LC', 'AD', 'SB', 'KC', \
           'BP', 'CD', 'CT', 'HG', 'HO', 'LH', 'RB', 'S', 'SI', 'W']


connection = pyodbc.connect('DRIVER={NetezzaSQL};SERVER=98.6.73.34;DATABASE=QA_RESEARCH;UID=pjangyodsuk;PWD=Iolap3goPJ')
cs = connection.cursor()


for SecurityID in SecurityIDs:

    print "-------------------- %s -----------------------" % (Markets[SecurityID])
    starttime = time.time()
    
    query = """ CREATE TEMP TABLE IF NOT Exists ADMIN.PM_CALCULATION_DAILYACCURACY
                ( SECURITYID INTEGER,
                  COST DOUBLE PRECISION,
                  TESTDATE DATE,
                  ACCURACY INTEGER ) DISTRIBUTE ON RANDOM; """
    cs.execute( query )
    cs.commit()
    connection.commit()
    print "Finish CREATING TEMP TABLE: PM_CALCULATION_DAILYACCURACY."
    
    query = """ CREATE TEMP TABLE IF NOT Exists ADMIN.PM_CALCULATION_THRESHOLD
                ( SECURITYID INTEGER,
                  FORMONTH DATE,
                  LOOKBACK INTEGER,
                  BESTTRHESHOLD DOUBLE PRECISION,
                  BESTACCURACY DOUBLE PRECISION ) DISTRIBUTE ON RANDOM; """
    cs.execute( query )
    cs.commit()
    connection.commit()
    print "Finish CREATING TEMP TABLE: PM_CALCULATION_THRESHOLD."
    
    query = """ CREATE TEMP TABLE IF NOT Exists ADMIN.PM_CALCULATION_ALLLOOKBACK_PREDICTIONS
                ( SECURITYID INTEGER,
                  FORMONTH DATE,
                  LOOKBACK INTEGER,
                  THRESHOLD DOUBLE PRECISION,
                  TESTDATE DATE,
                  ONOFF INTEGER ) DISTRIBUTE ON RANDOM; """
    cs.execute( query )
    cs.commit()
    connection.commit()
    print "Finish CREATING TEMP TABLE: PM_CALCULATION_ALLLOOKBACK_PREDICTIONS."
    
    query = """ CREATE TEMP TABLE IF NOT Exists ADMIN.PM_CALCULATION_MONTHLYACCURACY
                ( SECURITYID INTEGER,
                  FORMONTH DATE,
                  LOOKBACK INTEGER,
                  ACCURACY DOUBLE PRECISION ) DISTRIBUTE ON RANDOM; """
    cs.execute( query )
    cs.commit()
    connection.commit()
    print "Finish CREATING TEMP TABLE: PM_CALCULATION_MONTHLYACCURACY."
    
    query = """ INSERT INTO ADMIN.PM_CALCULATION_DAILYACCURACY
                SELECT input.SECURITYID, output.Cost, '1970-01-01'::date + output.TestDate * interval '1 second' As TestDate, output.Accuracy
                FROM (
                        SELECT p.MARKETID AS SECURITYID, extract(epoch from d.PROCESSDAY) as LONGPROCESSDAY, extract(epoch from d.STARTDAY) as LONGSTARTDAY, extract(epoch from p.RptDay) as LONGRptDay, p.CLOSETOCLOSEPNL, row_number() over (partition by d.PROCESSDAY order by p.RptDay) as rown
                        FROM ADMIN.MARKETDAILYPNL AS p
                        JOIN ADMIN.PM_PROCESSDAY AS d
                          ON p.RptDay < d.PROCESSDAY
                        WHERE p.MARKETID = %d AND d.PROCESSTYPE = 0 AND d.PROCESSDAY >= '2009-09-01') AS input,
                        TABLE WITH FINAL
                                ( PM_GetCostAccuracy(input.LONGPROCESSDAY, input.LONGSTARTDAY, input.LONGRptDay, CAST(input.CLOSETOCLOSEPNL AS Double), rown) ) AS output; """ % (SecurityID)
    cs.execute( query )
    cs.commit()
    connection.commit()
    print "Finish Calculating Daily Accuracy."
    
    query = """ INSERT INTO ADMIN.PM_CALCULATION_THRESHOLD
                SELECT input.SECURITYID, input.PROCESSDAY, input.PROCESSTYPE, output.BestThreshold, output.BestAccuracy
                FROM (
                        SELECT c.SecurityID, extract(epoch from c.TestDate) as LONGTESTDATE, d.PROCESSDAY, c.Cost, c.Accuracy, d.PROCESSTYPE, d.PROCESSID, row_number() over (partition by d.PROCESSID order by c.TestDate) as rown, count(*) over (partition by d.PROCESSID) as totalrow
                        FROM ADMIN.PM_CALCULATION_DAILYACCURACY AS c
                        JOIN ADMIN.PM_PROCESSDAY AS d
                          ON c.TestDate >= d.STARTDAY AND c.TestDate <= d.PROCESSDAY
                        WHERE c.SECURITYID = %d AND d.PROCESSTYPE >= 1 AND d.PROCESSTYPE <= 3 AND d.PROCESSDAY >= '2007-11-30') AS input,
                        TABLE WITH FINAL
                                ( PM_GetThreshold(input.LONGTESTDATE, CAST(input.Cost AS Double), input.Accuracy, input.rown, input.totalrow) ) AS output; """ % (SecurityID)
    cs.execute( query )
    cs.commit()
    connection.commit()
    print "Finish Calculating Threshold."
    
    query = """ INSERT INTO ADMIN.PM_CALCULATION_ALLLOOKBACK_PREDICTIONS
                SELECT input.SECURITYID, input.PROCESSDAY, input.LOOKBACK, input.BESTTRHESHOLD, '1970-01-01'::date + output.TestDate * interval '1 second' As TestDate, output.OnOff
                FROM (
                SELECT t.SECURITYID, d.STARTDAY, extract(epoch from d.STARTDAY) as LONGSTARTDAY, p.RPTDAY, extract(epoch from p.RPTDAY) as LONGRPTDAY, p.CLOSETOCLOSEPNL, t.BESTTRHESHOLD, d.PROCESSDAY, d.PROCESSID, t.LOOKBACK, row_number() over (partition by d.PROCESSID order by LONGRPTDAY) as rown, count(*) over (partition by d.PROCESSID) as totalrow
                FROM ADMIN.MARKETDAILYPNL AS p
                JOIN ADMIN.PM_PROCESSDAY AS d
                  ON p.RptDay <= d.PROCESSDAY
                JOIN ADMIN.PM_PREVIOUS_MONTH AS prev
                  ON d.PROCESSDAY = prev.PROCESSDAY
                JOIN ADMIN.PM_CALCULATION_THRESHOLD AS t
                  ON prev.PREVIOUSMONTH = t.FORMONTH AND t.LOOKBACK = d.PROCESSTYPE-3 AND p.MARKETID = t.SECURITYID
                WHERE p.MARKETID = %d AND d.PROCESSTYPE >= 4 AND d.PROCESSTYPE <= 6 AND d.PROCESSDAY >= '2009-12-31') AS input,
                        TABLE WITH FINAL
                                ( PM_GetExpectancyScore(input.LONGSTARTDAY, input.LONGRPTDAY, CAST(input.CLOSETOCLOSEPNL AS Double), CAST(input.BESTTRHESHOLD AS Double), input.rown, input.totalrow) ) AS output; """ % (SecurityID)
    cs.execute( query )
    cs.commit()
    connection.commit()
    print "Finish Calculating All Prediction."
    
    query = """ INSERT INTO ADMIN.PM_CALCULATION_MONTHLYACCURACY
                SELECT input.SECURITYID, '1970-01-01'::date + output.ForMonth * interval '1 second' As ForMonth, input.LOOKBACK, output.Accuracy
                FROM (
                SELECT pred.SECURITYID, d.PROCESSDAY, extract(epoch from d.PROCESSDAY) as LONGPROCESSDAY, pred.LOOKBACK, pred.TESTDATE, pred.ONOFF, pnl.CLOSETOCLOSEPNL, d.PROCESSID, row_number() over (partition by d.PROCESSID order by pred.TESTDATE) as rown, count(*) over (partition by d.PROCESSID) as totalrow
                FROM ADMIN.PM_CALCULATION_ALLLOOKBACK_PREDICTIONS AS pred
                JOIN ADMIN.MARKETDAILYPNL AS pnl
                  ON pred.TESTDATE = pnl.RPTDAY AND pred.SECURITYID = pnl.MARKETID
                JOIN ADMIN.PM_PROCESSDAY AS d
                  ON pred.TESTDATE >= d.STARTDAY AND pred.TESTDATE <= d.PROCESSDAY AND pred.LOOKBACK = d.PROCESSTYPE - 3
                WHERE pnl.MARKETID = %d AND d.PROCESSTYPE >= 4 AND d.PROCESSTYPE <= 6 AND d.PROCESSDAY >= '2009-12-31') AS input,
                TABLE WITH FINAL
                                ( PM_GetAccuracy(input.LONGPROCESSDAY, CAST(input.CLOSETOCLOSEPNL AS Double), input.OnOff, input.rown, input.totalrow) ) AS output; """ % (SecurityID)
    cs.execute( query )
    cs.commit()
    connection.commit()
    print "Finish Calculating Monthly Accuracy."

    query = """ SELECT pred.TESTDATE, pred.ONOFF, pl.CLOSETOCLOSEPNL AS PNL, pred.THRESHOLD, pred.LOOKBACK
                FROM ADMIN.PM_CALCULATION_ALLLOOKBACK_PREDICTIONS AS pred
                JOIN ADMIN.PM_PREVIOUS_MONTH AS prev
                  ON prev.ProcessDay = pred.Formonth
                JOIN (
                        SELECT A1.SecurityID, A1.ForMonth, MIN(A1.LookBack) AS BestLookBack
                        FROM ADMIN.PM_CALCULATION_MONTHLYACCURACY AS A1
                        JOIN (
                                SELECT SecurityID, ForMonth, MAX(Accuracy) AS MaxAccuracy
                                  FROM ADMIN.PM_CALCULATION_MONTHLYACCURACY
                                  GROUP BY SecurityID, ForMonth) AS A2
                          ON A1.SECURITYID = A2.SecurityID AND A1.FORMONTH = A2.ForMonth AND A1.ACCURACY = A2.MaxAccuracy
                         GROUP BY A1.SecurityID, A1.ForMonth
                        ) AS r
                  ON r.ForMonth = prev.PREVIOUSMONTH AND pred.LOOKBACK = r.BestLookBack AND pred.SECURITYID = r.SecurityID
                JOIN ADMIN.MARKETDAILYPNL AS pl
                  ON pl.MARKETID = pred.SECURITYID AND pl.RptDay = pred.TESTDATE
                WHERE pred.SECURITYID = %d AND pred.TestDate >= '2010-01-01'
                ORDER BY pred.TESTDATE """ % (SecurityID)
    cs.execute( query )
    results = cs.fetchall()
    print "Finish Retrieving Test Results."


    
    query = """ DROP TABLE ADMIN.PM_CALCULATION_DAILYACCURACY; """
    cs.execute( query )
    cs.commit()
    connection.commit()
    print "Finish dropping table PM_CALCULATION_DAILYACCURACY."
    
    query = """ DROP TABLE ADMIN.PM_CALCULATION_THRESHOLD; """
    cs.execute( query )
    cs.commit()
    connection.commit()
    print "Finish dropping table PM_CALCULATION_THRESHOLD."
    
    query = """ DROP TABLE ADMIN.PM_CALCULATION_ALLLOOKBACK_PREDICTIONS; """
    cs.execute( query )
    cs.commit()
    connection.commit()
    print "Finish dropping table PM_CALCULATION_ALLLOOKBACK_PREDICTIONS."
    
    query = """ DROP TABLE ADMIN.PM_CALCULATION_MONTHLYACCURACY; """
    cs.execute( query )
    cs.commit()
    connection.commit()
    print "Finish dropping table PM_CALCULATION_MONTHLYACCURACY."

    date = [ '' ]
    marketEquity = [ 0.0 ]
    modelEquity = [ 0.0 ]
    with open("%s/%s_Netezza.csv" % (resultDir, Markets[SecurityID]), "wb") as ofile:
        ofile.write("TestDate,OnOff,PNL,MarketEquity,ModelEquity,Threshold,Lookback\n")
        for record in results:
            date.append( record[0].strftime("%Y-%m-%d") )
            marketEquity.append( marketEquity[-1] + float(record[2]) )
            modelEquity.append( modelEquity[-1] + (float(record[2]) * int(record[1])) )
            ofile.write("%s,%d,%f,%f,%f,%f,%d\n" % (record[0].strftime("%Y-%m-%d"), record[1], record[2], marketEquity[-1], modelEquity[-1], record[3], record[4]))

    date = date[1:]
    marketEquity = marketEquity[1:]
    modelEquity = modelEquity[1:]

    NSep = len(date) / 10
    dateplot = [ ]
    for i in range(0, len(date), NSep):
        dateplot.append( date[i] )
    
    with PdfPages("%s/%s_Netezza.pdf" % (resultDir, Markets[SecurityID])) as pdf:
        plt.rcParams.update({'font.size': 8})
        fig = plt.figure()
        ax = fig.add_axes((.1,.3,.8,.6))
        """ PLOT Market Equity """
        plt.plot(range(0, len(marketEquity)), marketEquity, 'k', linewidth=0.2)
        plt.hold(True)
        """ PLOT Model Equity """
        plt.plot(range(0, len(modelEquity)), modelEquity, 'b', linewidth=0.2)
        ax.ticklabel_format(useOffset=False)
        plt.title("%s: Market VS PM" % (Markets[SecurityID]))
        plt.xlabel('Date')
        plt.ylabel('Equity ($)')
        plt.xticks(range(0, len(date), NSep), dateplot, rotation='vertical')
        plt.hold(False)
        pdf.savefig(fig)
        plt.close()
            

    print "Elapse testing time: %f" % (time.time() - starttime)

cs.close()
del cs
connection.close()
