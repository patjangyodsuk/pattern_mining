import csv
import json
import pyodbc
import calendar
from datetime import datetime


""" INITIALIZE """
ResultFolder = './Results_QA/CSV'
ModelID = 2018
Bot_VersionID = 1
SecurityIDs = range(1,5) + range(6,11) + range(12,15)


def getPreviousMonthEndDate(month, year):
    month -= 1
    if month == 0:
        month = 12
        year -= 1
    s, e = calendar.monthrange(year, month)
    return getEndDate(month, year)

def getEndDate(month, year):
    s, e = calendar.monthrange(year, month)
    return "%d-%02d-%02d" % (year, month, e)


connection = pyodbc.connect('DRIVER={NetezzaSQL};SERVER=98.6.73.34;DATABASE=RESEARCH;UID=pjangyodsuk;PWD=Iolap3goPJ')
cs = connection.cursor()


""" GET BOT IDS """
query = """ SELECT BOTID, MARKETID, BOTNAME
            FROM ADMIN.BOTS
            WHERE BOTNAME LIKE 'PM_%%_BackTesting' AND MODELID = %d AND VERSIONID = %d
            ORDER BY MARKETID; """ % (ModelID, Bot_VersionID)
cs.execute( query )
data = cs.fetchall()

BotIDs = {}
MarketNames = {}
for record in data:
    BotIDs.update( { int(record[1]): int(record[0]) } )
    MarketNames.update( { int(record[1]): record[2][4:record[2].index('.')] } )
    

for SecurityID in SecurityIDs:

    """ READ RESULT DATA """
    data = []
    with open("%s/%s_Netezza.csv" % (ResultFolder, MarketNames[SecurityID]), "rb") as infile:
        reader = csv.reader(infile, delimiter=',')
        next(reader, None)
        data = list(reader)

    previous_position = 0
    previous_threshold = -1

    Transactions = []
    Parameters = []
    
    for record in data:
        date = datetime.strptime(record[0], "%Y-%m-%d")
        position = int(record[1])
        threshold = float(record[5])
        lookback = int(record[6])
        unit = position - previous_position
        

        """ HAVE TRANSACTION WHEN POSITION CHANGES """
        if position != previous_position:
            Transactions.append( [BotIDs[SecurityID], SecurityID, date, unit, position] )

        """ THRESHOLD CHANGES -> KEEP PREVIOUS THRESHOLD TO DB """
        if threshold != previous_threshold:
            trainfrom = '2002-01-01'
            trainto = getPreviousMonthEndDate(date.month, date.year)
            usefrom = "%d-%02d-01" % (date.year, date.month)
            useto = getEndDate(date.month, date.year)
            hyperparam = json.dumps( { 'Threshold': threshold, 'Lookback': lookback } )
            created = "%s 00:00:00" % (trainto)
            Parameters.append( [BotIDs[SecurityID], trainfrom, trainto, usefrom, useto, hyperparam, created] )

        previous_position = position
        previous_threshold = threshold

    """ UPLOAD TRANSATIONS """
    query = """ INSERT INTO ADMIN.BOTSTRANSACTIONS
                (BOTID, MARKETID, TRANSACTIONDATE, UNITS, EXPOSURE)
                VALUES (%d, %d, '%s', %d, %d); """
    for record in Transactions:
        cs.execute( query % (record[0], record[1], record[2], record[3], record[4]) )
        cs.commit()
        connection.commit()

    """ UPLOAD PARAMETERS """
    query = """ INSERT INTO ADMIN.BOTSMODELPARAMETERS
                (BOTID, TRAINFROM, TRAINTO, USEFROM, USETO, HYPERPARAMETERS, OUTPUTPARAMETERS, CREATED)
                VALUES (%d, '%s', '%s', '%s', '%s', '%s', '', '%s'); """
    for record in Parameters:
        cs.execute( query % (record[0], record[1], record[2], record[3], record[4], record[5], record[6]) )
        cs.commit()
        connection.commit()

    


cs.close()
del cs
connection.close()
