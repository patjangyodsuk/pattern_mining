INSERT INTO DEV_CAPITALOGIX.ADMIN.PM_CALCULATION_THRESHOLD
SELECT input.SECURITYID, input.PROCESSDAY, input.PROCESSTYPE, output.BestThreshold, output.BestAccuracy
FROM (
	SELECT c.SecurityID, extract(epoch from c.TestDate) as LONGTESTDATE, d.PROCESSDAY, c.Cost, c.Accuracy, d.PROCESSTYPE, d.PROCESSID, row_number() over (partition by d.PROCESSID order by c.TestDate) as rown, count(*) over (partition by d.PROCESSID) as totalrow
	FROM DEV_CAPITALOGIX.ADMIN.PM_CALCULATION_DAILYACCURACY AS c
	JOIN DEV_CAPITALOGIX.ADMIN.PM_PROCESSDAY AS d
	  ON c.TestDate >= d.STARTDAY AND c.TestDate <= d.PROCESSDAY
	WHERE c.SECURITYID = 12 AND d.PROCESSTYPE >= 1 AND d.PROCESSTYPE <= 3) AS input,
	TABLE WITH FINAL
		( PM_GetThreshold(input.LONGTESTDATE, input.Cost, input.Accuracy, input.rown, input.totalrow) ) AS output;

