INSERT INTO DEV_CAPITALOGIX.ADMIN.PM_CALCULATION_MONTHLYACCURACY
SELECT input.SECURITYID, '1970-01-01'::date + output.ForMonth * interval '1 second' As ForMonth, input.LOOKBACK, output.Accuracy
FROM (
SELECT pred.SECURITYID, d.PROCESSDAY, extract(epoch from d.PROCESSDAY) as LONGPROCESSDAY, pred.LOOKBACK, pred.TESTDATE, pred.ONOFF, pnl.PNL, d.PROCESSID, row_number() over (partition by d.PROCESSID order by pred.TESTDATE) as rown, count(*) over (partition by d.PROCESSID) as totalrow
FROM DEV_CAPITALOGIX.ADMIN.PM_CALCULATION_ALLLOOKBACK_PREDICTIONS AS pred
JOIN DEV_CAPITALOGIX.ADMIN.MARKETPNL AS pnl
  ON pred.TESTDATE = pnl.RPTDAY AND pred.SECURITYID = pnl.SECURITYID
JOIN DEV_CAPITALOGIX.ADMIN.PM_PROCESSDAY AS d
  ON pred.TESTDATE >= d.STARTDAY AND pred.TESTDATE <= d.PROCESSDAY AND pred.LOOKBACK = d.PROCESSTYPE - 3
WHERE d.PROCESSTYPE >= 4 AND d.PROCESSTYPE <= 6) AS input,
TABLE WITH FINAL
		( PM_GetAccuracy(input.LONGPROCESSDAY, input.PNL, input.OnOff, input.rown, input.totalrow) ) AS output;