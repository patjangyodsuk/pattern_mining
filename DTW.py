import math

class DTW:

    def __init__(self, q, s, lb):
        self.query = q
        self.len_q = len(q)
        self.seq = s
        self.len_s = len(s)
        self.LB = lb
        self.thres = float('inf')

    def initSakoeChibaBand(self, w):
        self.SCwidth = int(w/2)

    def setThreshold(self, th):
        self.thres = th

    def initDTW(self):
        """ dtw[r][c] -> col-seq , row -> query """
        """ first column and first row are inf except dtx[0][0] = 0 """
        first_col = [ float('inf') ]
        for i in range(0, self.len_s):
            first_col.append(float('inf'))
            
        self.dtw = [ first_col ]
        for i in range(0, self.len_q):
            col = [ float('inf') ]
            for j in range(0, max(0, i-self.SCwidth)):
                col.append( float('inf') )
            for j in range(max(0, i-self.SCwidth), min(i+self.SCwidth+1, self.len_s)):
                col.append(0.0)
            for j in range(min(i+self.SCwidth+1, self.len_s), self.len_s):
                col.append( float('inf') )
            self.dtw.append(col)

        self.dtw[0][0] = 0.0

    def compute(self):
        for q in range(0, self.len_q):
            dist_w_lb = [ float('inf') ]
            for s in range(0, max(0, q-self.SCwidth)):
                self.dtw[q+1][s+1] = float('inf')
            for s in range(max(0, q-self.SCwidth), min(q+self.SCwidth+1, self.len_s)):
                dist = math.pow(self.query[q] - self.seq[s], 2)
                self.dtw[q+1][s+1] = min(self.dtw[q+1][s], self.dtw[q][s+1], self.dtw[q][s]) + dist
                dist_w_lb.append( self.dtw[q+1][s+1] + sum(self.LB[s+1:]) )
            for s in range(min(q+self.SCwidth+1, self.len_s), self.len_s):
                self.dtw[q+1][s+1] = float('inf')
            """ EARLY ABANDONING :: STOP WHEN BEST-SO-FAR > THRESHOLD """
            if min(dist_w_lb) > self.thres: #min(self.dtw[q+1]) > self.thres:
                self.dtw[self.len_q][self.len_s] = float('inf')
                break
        
        self.cost = self.dtw[self.len_q][self.len_s]

    def printTab(self):
        for i in range(0, len(self.dtw)):
            row = ""
            for j in range(0, len(self.dtw[i])):
                row += "%.2f, " % (self.dtw[i][j])
            print row + "\n"
            
