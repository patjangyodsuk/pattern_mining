import numpy
from ZNorm import ZNorm
from LowerBound import LowerBound
from DTW import DTW


class PatternMatching:

    def __init__(self, q, w, th):
        self.window = w
        self.threshold = th
        self.Matches = []
        """ NORMALIZE QUERY """
        znorm = ZNorm(q)
        self.normed_query = znorm.compute()
        """ INIT LOWER BOUND ON QUERY """
        self.LB_Query = LowerBound(self.normed_query, self.window)
        self.LB_Query.computeUL()

    def getMatchedPatterns(self, sequence):
        QLength = len(self.normed_query)
        i = 0
        while i < len(sequence)-QLength:
            foundMatch = False
            for length in range(80, 35, -5):
                if i+length > len(sequence):
                    continue
                if length == QLength:
                    subseq = sequence[i:i+length]
                else:
                    subseq = numpy.interp(numpy.linspace(0, 1, QLength), numpy.linspace(0, 1, length), sequence[i:i+length])
                znorm = ZNorm(subseq)
                normed_subseq = znorm.compute()
                if self.LB_Query.calcLBKimFL(normed_subseq) < self.threshold:
                    if self.LB_Query.calcLBKeogh(normed_subseq) < self.threshold:
                        LB_Subseq = LowerBound(normed_subseq, self.window)
                        LB_Subseq.computeUL()
                        if LB_Subseq.calcLBKeogh(self.normed_query) < self.threshold:
                            dtw = DTW(self.normed_query, normed_subseq, self.LB_Query.LBKeogh)
                            dtw.initSakoeChibaBand(self.window)
                            dtw.setThreshold(self.threshold)
                            dtw.initDTW()
                            dtw.compute()
                            if dtw.cost < self.threshold:
                                self.Matches.append( {'index':range(i,i+length), 'seq':sequence[i:i+length], 'cost':dtw.cost} )
                                foundMatch = True
                                #i += (self.window - 1)
                                #break
            if foundMatch:
                i += (self.window - 1)
            i += 1
        return self.Matches
