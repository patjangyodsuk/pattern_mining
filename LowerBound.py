import math

class LowerBound:

    def __init__(self, boundseq, w):
        self.boundseq = boundseq
        self.SCwidth = int(w/2)
        self.U = []
        self.L = []
        self.length = len(boundseq)

    def computeUL(self):
        for i in range(0, self.length):
            self.U.append( max(self.boundseq[max(0,i-self.SCwidth):min(self.length,i+self.SCwidth+1)]) )
            self.L.append( min(self.boundseq[max(0,i-self.SCwidth):min(self.length,i+self.SCwidth+1)]) )

    def calcLBKimFL(self, seq):
        return max( abs(self.boundseq[0]-seq[0]), abs(self.boundseq[-1]-seq[-1]) )

    def calcLBKeogh(self, seq):
        self.LBKeogh = []
        for i in range(0, self.length):
            if seq[i] > self.U[i]:
                self.LBKeogh.append( math.pow(self.U[i]-seq[i], 2) )
            elif seq[i] < self.L[i]:
                self.LBKeogh.append( math.pow(self.L[i]-seq[i], 2) )
            else:
                self.LBKeogh.append( 0.0 )
        return sum(self.LBKeogh)
