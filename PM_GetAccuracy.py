import nzae


class PM_GetAccuracy(nzae.Ae):

    PNL = []
    OnOff = []

    def _runUdtf(self):

        for row in self:

            processday, pnl, onoff, rown, totalrow = row

            if rown == 1:
                self.PNL = []
                self.OnOff = []


            self.PNL.append( pnl )
            self.OnOff.append( onoff )

            if rown == totalrow:

                accuracy = 0.0
                for i in range(0, len(self.PNL)):
                    if (self.PNL[i] > 0 and self.OnOff[i] == 1) or (self.PNL[i] <= 0 and self.OnOff[i] == -1) or (self.PNL[i] <= 0 and self.OnOff[i] == 0):
                        accuracy += 1.0
                accuracy /= totalrow

                self.output(processday, accuracy)
        
        

PM_GetAccuracy.run()
