INSERT INTO DEV_CAPITALOGIX.ADMIN.PM_CALCULATION_DAILYACCURACY
SELECT input.SECURITYID, output.Cost, '1970-01-01'::date + output.TestDate * interval '1 second' As TestDate, output.Accuracy
FROM (
	SELECT p.SECURITYID, extract(epoch from d.PROCESSDAY) as LONGPROCESSDAY, extract(epoch from d.STARTDAY) as LONGSTARTDAY, extract(epoch from p.RptDay) as LONGRptDay, p.PNL, row_number() over (partition by d.PROCESSDAY order by p.RptDay) as rown
	--SELECT p.SECURITYID, d.PROCESSDAY, extract(epoch from d.PROCESSDAY) as LONGPROCESSDAY, p.RptDay, extract(epoch from p.RptDay) as LONGRptDay, p.PNL, row_number() over (partition by d.PROCESSDAY order by p.RptDay) as rown
	FROM DEV_CAPITALOGIX.ADMIN.MARKETPNL AS p
	JOIN DEV_CAPITALOGIX.ADMIN.PM_PROCESSDAY AS d
	  ON p.RptDay < d.PROCESSDAY
	WHERE p.SECURITYID = 12 AND d.PROCESSTYPE = 0) AS input,
	TABLE WITH FINAL
		( PM_GetCostAccuracy(input.LONGPROCESSDAY, input.LONGSTARTDAY, input.LONGRptDay, CAST(input.PNL AS Double), rown) ) AS output